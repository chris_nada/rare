# RaRe

**RAndom REnamer.** 

_Tool that random-renames all files in the current directory._

+ Written in pure C++17.
+ Requires a compiler that supports `std::filesystem`.
+ Random filenames are a 16-digit (C++) hexadecimal hash.
+ Add it to your PATH to run it in a console anywhere.
