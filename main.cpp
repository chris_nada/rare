#include <iostream>
#include <filesystem>
#include <algorithm>

int main() {
    std::hash<std::string> hash_fun;

    for (auto& path : std::filesystem::directory_iterator(std::filesystem::current_path())) {
        try {
            if (path.is_regular_file()) {
                std::string pfad(path.path());
                if (pfad.find("rare") != std::string::npos) continue;

                // Den Pfad hashen
                std::size_t hash = hash_fun(path.path());

                // Zahl als Hex ausgeben
                std::stringstream ss;
                ss.setf(std::ios::hex, std::ios::basefield);

                // Dateiendung erhalten (ohne ")
                std::string ext = path.path().extension();
                ext.erase(std::remove(ext.begin(), ext.end(), '"'), ext.end());

                // Neuen Namen generieren und schreiben
                ss << hash << ext << std::flush;
                std::filesystem::rename(path, ss.str());
            }
        }
        catch (...) { }
    }
    return 0;
}
